<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/view/{id}', 'HomeController@jobView')->name('view');
Route::get('/categories', 'HomeController@jobCategories')->name('jobs-categories');
Route::get('/categories/{id}', 'HomeController@allJobsByCategory')->name('jobs-categories-id');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'HomeController@autocomplete'));
Route::get('/job/search','HomeController@searchComplete')->name('jobs.search');

Route::get('/user/logout', 'auth\LoginController@userlogout')->name('user.logout');


// Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('thegate')->namespace('Admin')->group(function(){
	//Dashboard=====================================
	Route::get('/','DashboardController@index')->name('thegate.dashboard');
	//Admin Login-Logout=========================
	Route::get('/login','LoginController@show')->name('thegate.login');
	Route::post('/login','LoginController@login')->name('thegate.login.submit');
	Route::get('/logout', 'LoginController@logout')->name('thegate.logout');

	//Job_Categories==============================
	Route::get('job_categories','JobCategoriesController@index')->name('job_categories.index');
	Route::get('job_categories/create','JobCategoriesController@create')->name('job_categories.create');
	Route::post('job_categories','JobCategoriesController@store')->name('job_categories.submit');
	Route::get('job_categories/{id}','JobCategoriesController@edit')->name('job_categories.edit');
	Route::post('job_categories/update/{id}','JobCategoriesController@update')->name('job_categories.update');
	Route::post('job_categories/delete','JobCategoriesController@delete')->name('job_categories.delete');

	//Job Location===============================
	Route::get('job_location','JobLocationsController@index')->name('job_location.index');
	Route::get('job_location/create','JobLocationsController@create')->name('job_location.create');
	Route::post('job_location','JobLocationsController@store')->name('job_location.submit');
	Route::get('job_location/{id}','JobLocationsController@edit')->name('job_location.edit');
	Route::post('job_location/update/{id}','JobLocationsController@update')->name('job_location.update');
	Route::post('job_location/delete','JobLocationsController@delete')->name('job_location.delete');


	//Job Schedule===============================
	Route::get('job_schedule','JobScheduleController@index')->name('job_schedule.index');
	Route::get('job_schedule/create','JobScheduleController@create')->name('job_schedule.create');
	Route::post('job_schedule','JobScheduleController@store')->name('job_schedule.submit');
	Route::get('job_schedule/{id}','JobScheduleController@edit')->name('job_schedule.edit');
	Route::post('job_schedule/update/{id}','JobScheduleController@update')->name('job_schedule.update');
	Route::post('job_schedule/delete','JobScheduleController@delete')->name('job_schedule.delete');

	//Job Company===============================
	Route::get('companies','CompanyController@index')->name('companies.index');
	Route::get('companies/create','CompanyController@create')->name('companies.create');
	Route::post('companies','CompanyController@store')->name('companies.submit');
	Route::get('companies/{id}','CompanyController@edit')->name('companies.edit');
	Route::post('companies/update/{id}','CompanyController@update')->name('companies.update');
	Route::post('companies/delete','CompanyController@delete')->name('companies.delete');
	Route::post('companies/approved/{id}','CompanyController@approved')->name('comapnies.approved');


	//Job Post===============================
	Route::get('post_job/admin','PostJobController@admin')->name('post_job.admin.index');
	Route::get('post_job/client','PostJobController@client')->name('post_job.client.index');
	Route::get('post_job/create','PostJobController@create')->name('post_job.create');
	Route::post('post_job','PostJobController@store')->name('post_job.submit');
	Route::get('post_job/{id}','PostJobController@edit')->name('post_job.edit');
	Route::post('post_job/update/{id}','PostJobController@update')->name('post_job.update');
	Route::post('post_job/delete','PostJobController@delete')->name('post_job.delete');

	Route::post('post_job/job/{id}','PostJobController@approve')->name('post_job.approve');

	//User cliden====================================
	Route::get('user/client','UserClientController@index')->name('user.client.index');
	Route::post('user/client/delete','UserClientController@delete')->name('user.client.delete');
	Route::post('user/client/type/client','UserClientController@client')->name('user.client.update.client');
	Route::post('user/client/type/admin','UserClientController@admin')->name('user.client.update.admin');
	//Order=========================
	



	// Management=====================================
	Route::get('user/admin','UserAdminController@index')->name('user.admin');
	Route::get('user/admin/create','UserAdminController@create')->name('user.admin.create');
	Route::post('user/admin/create','UserAdminController@store')->name('user.admin.create.submit');
	Route::get('user/admin/edit/{id}','UserAdminController@edit')->name('user.admin.edit');
	Route::post('user/admin/update/{id}','UserAdminController@update')->name('user.admin.update');
	Route::post('user/admin/delete','UserAdminController@delete')->name('user.admin.delete');

});



//user Dashbaord client
Route::prefix('user')->name('user.')->group(function () {
    Route::get('/', ['as' => 'index', 'uses' => 'UserDashboardController@index']); 
});
Route::prefix('user')->group(function () {

	//Companies================================================
	Route::get('companies','UserCompanyController@index')->name('user.company'); 
	Route::get('companies/create', 'UserCompanyController@create')->name('user.create.company');
	Route::post('companies/post', 'UserCompanyController@store')->name('user.submit.company'); 
	Route::get('companies/edit/{id}', 'UserCompanyController@edit')->name('user.edit.company'); 
	Route::post('companies/update/{id}','UserCompanyController@update')->name('user.update.company');
	Route::post('companies/delete','UserCompanyController@delete')->name('user.delete.company');

	//Job ===============================================================================	
	Route::post('job/delete','UserJobsController@delete')->name('user.job.delete');

	//CV=============================
	Route::get('job/applied','ApplyJobController@getapply')->name('user.apply.job.index');
	Route::get('view/cv/{id}','ApplyJobController@viewcv')->name('view.cv');
	Route::post('view/cv/delete','ApplyJobController@delete')->name('view.cv.deletes');




	
	
});
Route::prefix('user/jobs')->name('user.')->group(function () {
	Route::get('/', ['as' => 'job.index', 'uses' => 'UserJobsController@index']); 
	Route::get('/create', ['as' => 'job.create', 'uses' => 'UserJobsController@create']); 
	Route::post('/store', ['as' => 'job.store', 'uses' => 'UserJobsController@store']); 
	Route::get('/edit/{id}', ['as' => 'job.edit', 'uses' => 'UserJobsController@edit']); 
	Route::post('/update/{id}', ['as' => 'job.update', 'uses' => 'UserJobsController@update']); 

	
});

//Apply Job==============================================
Route::get('apply/job/{id}','ApplyJobController@apply')->name('user.cleint.apply.job');
Route::post('apply/job/submit','ApplyJobController@applysubmit')->name('user.clent.apply.job.submit');
