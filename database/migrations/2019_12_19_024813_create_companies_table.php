<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('company_name')->unique();
            $table->string('company_email')->unique();
            $table->string('company_phone_number')->unique();
            $table->string('company_website_url')->nullable();
            $table->text('company_description')->nullable();
            $table->text('company_address');
            $table->string('company_logo');
            $table->string('status',50)->default('Not approved');
            $table->integer('active')->default(1);
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
