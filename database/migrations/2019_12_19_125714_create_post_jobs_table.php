<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('post_by')->foreign('post_by')->references('id')->on('users');
            $table->integer('category_id')->foreign('category_id')->references('id')->on('job_categories');
            $table->integer('company_id')->foreign('company_id')->references('id')->on('companies');
            $table->integer('location_id')->foreign('location_id')->references('id')->on('job_locations');
            $table->integer('schedule_id')->foreign('schedule_id')->references('id')->on('job_schedules');
            $table->string('job_title');
            $table->decimal('salary',18,2)->default("00");
            $table->text('requirements');
            $table->text('duties');
            $table->date('post_date');
            $table->date('close_date');    
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_jobs');
    }
}
