<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategories extends Model
{
    
    protected $fillable = [
        'jobcategoriesname', 'stug', 'jobdescription',
    ];
    public function jobs(){
    	return $this->hasMany(PostJob::class,'category_id','id');
    }
   
}
