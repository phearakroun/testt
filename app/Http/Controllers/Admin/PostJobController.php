<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostJob;
use App\companies;
use App\JobCategories;
use App\JobLocation;
use App\JobSchedule;
use Auth;
use App\Http\Requests\admin\PostJobRequest;
use DB;
class PostJobController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:thegate');
    }

    public function admin()
    {
        $datas = DB::table('post_jobs')
            ->join('job_locations', 'post_jobs.location_id', '=', 'job_locations.id') 
            ->join('companies', 'post_jobs.company_id', '=', 'companies.id')
            ->join('users','post_jobs.post_by','=','users.id')       
            ->select('post_jobs.id','post_jobs.job_title',
                'companies.company_name',
                'job_locations.joblocation',
                'post_jobs.close_date',
                'post_jobs.status')
            ->where('users.type','admin')
            ->where('users.status','1')
            ->orderBy('post_jobs.id', 'desc')
            ->paginate(10);
        $data=array("data"=>$datas);
        // dd($data);
        return view('admin.job_post.admin.index',$data);
    }

    public function client()
    {

      
        $datas = DB::table('post_jobs')
        ->join('job_locations', 'post_jobs.location_id', '=', 'job_locations.id') 
        ->join('companies', 'post_jobs.company_id', '=', 'companies.id')
        ->join('users','post_jobs.post_by','=','users.id')       
        ->select('post_jobs.id','post_jobs.job_title',
            'companies.company_name',
            'job_locations.joblocation',
            'post_jobs.close_date',
            'post_jobs.status')
        ->where('users.type','client')
        ->where('users.status','1')
        ->orderBy('post_jobs.id', 'desc')
        ->paginate(10);
    $data=array("data"=>$datas);
        // dd($data);
        return view('admin.job_post.client.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=JobCategories::all();
        $schedule=JobSchedule::all();
        $company=companies::all();
        $location=JobLocation::all();

        $data=array("jocate"=>$category,"jobsched"=>$schedule,"comp"=>$company,"jobLoc"=>$location);

        return view('admin.job_post.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostJobRequest $request)
    {
    //    $date = date('d-m-Y H:i:s', strtotime($request->close_date));
        // dd($date);
        $post=new PostJob();
        $post->category_id = $request->categories ;
        $post->job_title = $request->jobtitle ;
        $post->company_id = $request->company;
        $post->location_id = $request->joblocation;
        $post->schedule_id = $request->schedule;
        $post->salary = $request->salary ;
        $post->close_date = date('Y-m-d H:i:s', strtotime($request->close_date)) ;
        $post->duties = $request->duties ;
        $post->requirements = $request->requirement ;
        $post->post_date = now();
        $post->post_by = Auth::user()->id;
        $post->save();
        return redirect()->route("post_job.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=PostJob::find($id);
        $category=JobCategories::all();
        $schedule=JobSchedule::all();
        $company=companies::all();
        $location=JobLocation::all();

        $data=array("postjob"=>$post,"jocate"=>$category,"jobsched"=>$schedule,"comp"=>$company,"jobLoc"=>$location);

        return view('admin.job_post.edit',$data);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostJobRequest $request, $id)
    {
        $post=PostJob::find($id);
        $post->category_id = $request->categories ;
        $post->job_title = $request->jobtitle ;
        $post->company_id = $request->company;
        $post->location_id = $request->joblocation;
        $post->schedule_id = $request->schedule;
        $post->salary = $request->salary ;
        $post->close_date = date('Y-m-d H:i:s', strtotime($request->close_date));
        $post->duties = $request->duties ;
        $post->requirements = $request->requirement ;
        // $post->post_date = now();
        // $post->post_by = Auth::user()->id;
        $post->save();
        return redirect()->route("post_job.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $post = PostJob::find ($request->id)->delete();

        return "Deleted";
    }

    public function approve(Request $request,$id){
        $app=PostJob::find($id);
        $app->status="1";
        $app->save();
        return back()->with("approve");
    }
}
