<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class UserClientController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:thegate');
    }

    public function index(){
    	$user = User::where('status',1)->orderBy('id', 'desc')->paginate(10);
    	$datas=array("data"=>$user);
    	return view('admin.user.client.index',$datas);
    }


    public function delete(Request $request){

        $user = User::find ($request->id);
        $user->status="0";
        $user->save();
       	
    	return "Success";
    }

    public function client(Request $request){
        $client=User::find($request->id);
        $client->type='admin';
        $client->save();
        return "Success";
    }

    public function admin(Request $request){
        $client=User::find($request->id);
        $client->type='client';
        $client->save();
        return "Success";
    }
}
