<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admins;
use App\Http\Requests\admin\UserAdminRequest;
use App\User;

class UserAdminController extends Controller

{
    public function index(){
    	$admin=Admins::where('type','admin')->get();
    	$data=[
    		"admin"=>$admin
    	];
    	return view('admin.user.admin.index',$data);
	}
	
	public function create(){
		return view('admin.user.admin.create');
	}

	public function store(UserAdminRequest $request	){
		$admin= new Admins();
		$admin->name=$request->name;
		$admin->email=$request->email;
		$admin->type="admin";
		$admin->password=bcrypt($request->password);
		$admin->save();
		return redirect()->route("user.admin");
	}

	public function edit($id){
		$user=Admins::find($id);

		$data=['data'=>$user];
		return view('admin.user.admin.edit',$data);
	}

	public function update(UserAdminRequest $request ,$id){
		$admin=Admins::find($id);
		$admin->name=$request->name;
		$admin->email=$request->email;
		$admin->type="admin";
		$admin->password=bcrypt($request->password);
		$admin->save();
		return redirect()->route("user.admin");
	}

	public function delete(Request $request){
		$post = Admins::find ($request->id)->delete();

        return "Deleted";
	}


}
