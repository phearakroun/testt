<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApplyJob;
use DB;

class ApplyJobController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth:thegate');
    }

    public function apply(){
       
        $datas = DB::table('apply_jobs')

            ->join('post_jobs', 'apply_jobs.job_id', '=', 'post_jobs.id')
            ->select('apply_jobs.*','post_jobs.job_title',)
            ->orderBy('apply_jobs.id', 'desc')
            ->get();


    
        $data=['data'=>$datas];
    	return view('admin.apply_job.index',$data);
    }

    public function viewcv($id){
        $file=ApplyJob::find($id);
         $data=array("pro"=>$file);
         return view('admin.apply_job.viewcv',$data);
    }

    public function delete(Request $request){

       
        $job=ApplyJob::find($request->id);
        $job->delete();

        return "Deleted";
    }
}
