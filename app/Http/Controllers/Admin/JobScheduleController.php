<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\JobScheduleRequest;
use App\JobSchedule;

class JobScheduleController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth:thegate');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JobSchedule::where('status','1')->orderBy('id', 'desc')->paginate(10);
        return view('admin.job_schedule.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job_schedule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobScheduleRequest $request)
    {
        $job = new JobSchedule();
        $job->schedule_name=$request->schedule_name;
        $job->slug=$request->slug;
        $job->save();
        return redirect()->route('job_schedule.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobs=JobSchedule::find($id);
        $data=array("job"=>$jobs);
        return View('admin.job_schedule.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'schedule_name' => 'required|string|max:255',
        ]);

        $job = JobSchedule::find($id);
        $job->schedule_name=$request->schedule_name;
        $job->slug=$request->slug;
        $job->save();
        return redirect()->route('job_schedule.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $data=JobSchedule::find($request->id);
        $data->status=0;
        $data->save();
        return "Deleted";
    }
}
