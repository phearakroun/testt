<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobCategories;
use App\Http\Requests\admin\JobCategoriesRequest;

class JobCategoriesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:thegate');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data = JobCategories::where('status','1')->orderBy('id', 'desc')->paginate(10);

        return view('admin.job_categories.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobCategoriesRequest $request)
    {
        $job = new JobCategories();
        $job->jobcategoriesname=$request->jobcategoriesname;
        $job->jobdescription=$request->description;
        $job->save();
        return redirect()->route('job_categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $jobs=JobCategories::find($id);
        $data=array("job"=>$jobs);
        return View('admin.job_categories.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'jobcategoriesname' => 'required|string|max:255'
        ]);

 
        $job = JobCategories::find($id);
        $job->jobcategoriesname=$request->jobcategoriesname;
        $job->jobdescription=$request->description;
        $job->save();
        return redirect()->route('job_categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

        $data=JobCategories::find($request->id);
        $data->status=0;
        $data->save();
        return "Deleted";
    }
}
