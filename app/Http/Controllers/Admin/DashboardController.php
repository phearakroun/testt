<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostJob;
use App\JobCategories;
use App\JobLocation;
use App\companies;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:thegate');
    }
    public function index(){
        $post=PostJob::count();
        
        $cate=JobCategories::where('status','1');
        $newcate=$cate->count();

        $locat=JobLocation::where('status','1');
        $newlocate=$locat->count(); 

        $comp=companies::where('status','1');
        $newcomp=$comp->count();

      $data=array(
        "post"=>$post,
        "cate"=>$newcate,
        "locat"=>$newlocate,
        "comp"=>$newcomp);
    	return view('Admin.dashboard',$data);
    }
}
