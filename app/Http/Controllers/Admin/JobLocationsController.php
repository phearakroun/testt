<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobLocation;
use App\Http\Requests\admin\JobLocationRequest;

class JobLocationsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth:thegate');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = JobLocation::where('status','1')->orderBy('id', 'desc')->paginate(10);
        return view('admin.job_location.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job_location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobLocationRequest $request)
    {
        $job = new JobLocation();
        $job->joblocation=$request->joblocation;
        $job->jobdescription=$request->description;
        $job->save();
        return redirect()->route('job_location.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobs=JobLocation::find($id);
        $data=array("job"=>$jobs);
        return View('admin.job_location.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'joblocation' => 'required|string|max:255',
        ]);


        $job = JobLocation::find($id);
        $job->joblocation=$request->joblocation;
        $job->jobdescription=$request->description;
        $job->save();
        return redirect()->route('job_location.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $data=JobLocation::find($request->id);
        $data->status=0;
        $data->save();
        return "Deleted";
    }
}
