<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\LoginRequest;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:thegate',['except'=>['logout']]);
    }

    
    public function show(){
    	return view('Admin.login');
    }

    public function login(Request $request){
        if(Auth::guard('thegate')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
    		return redirect()->intended(route('thegate.dashboard'));
    	}
    	return redirect()->back()->withInput($request->only('email','password'));
    }

    public function logout(){
        
        Auth::guard('thegate')->logout();
        return redirect('/thegate');  
    } 
}
