<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ApplyJobRequest;
use App\ApplyJob;
use DB;
use Auth;

class ApplyJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function apply($id){
        $job=$id;
        $data=["data"=>$job];
        return view('front.applyjob.form',$data);
    }

    public function applysubmit(ApplyJobRequest $request){
    
                $path=$request->file('document')->store('applyjob');
                $app=new ApplyJob();
    
                $app->document=$path;
                $app->phone=$request->phone;
                $app->job_id=$request->job_id;
                $app->save();
                return redirect()->route('home');
        
    }

    public function getapply(){

        $user=Auth::user();
      
       
        $datas = DB::table('apply_jobs')

            ->join('post_jobs', 'apply_jobs.job_id', '=', 'post_jobs.id')
            ->select('apply_jobs.*','post_jobs.job_title')
            ->orderBy('apply_jobs.id', 'desc')
            ->where('post_jobs.post_by',$user->id)
            ->get(); 
        $data=['data'=>$datas];
    	return view('front.applyjob..index',$data);
    }

    public function viewcv($id){
        $file=ApplyJob::find($id);
         $data=array("pro"=>$file);
         return view('front.applyjob..viewcv',$data);
    }

    public function delete(Request $request){

       
        $job=ApplyJob::find($request->id);
        $job->delete();

        return "Deleted";
    }




    
}
