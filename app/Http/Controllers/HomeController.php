<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PostJob;
use App\companies;
use App\JobCategories;
use App\JobLocation;
use App\JobSchedule;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function homepage(){
        $cat=JobCategories::all();

        // $data=array(
        // "categories"=>$cat)

        return view('welcome',$cat);
    }
    public function index(Request $request)
    {
        $f_name = isset($request->name) ? $request->name : "";
        $f_location = isset($request->location) ? $request->location : "";
        $f_category = isset($request->category) ? $request->category : "";
        $whereData=[];
        if($f_location != "") {
            $whereData[] = ['location_id','>=', $f_location];
        }
        if($f_category != "") {
            $whereData[] = ['category_id', '>=', $f_category];
        }
        if($f_name != "") {
            $whereData[] = ['job_title', 'LIKE' , "%".$f_name."%"];
        }
        $categories=JobCategories::all();
        $locations=JobLocation::all();
        $schedules=JobSchedule::all();
        $jobs=PostJob::where($whereData)
                        ->orderBy('id','DESC')
                        ->simplePaginate(30);
        $cats=JobCategories::all();
        // dd($whereData[0]);
        // dd($cats);
        $data = [
           'jobs'=>$jobs,
           'categories'=>$cats,
           'categories'=>$categories,
           'locations'=>$locations,
           'schedules'=>$schedules,
           'f_name'=>$f_name,
           'f_location'=>$f_location,
           'f_category'=>$f_category
        ];
        // return view('layouts.homemaster')->with($data);
        // dd();
        return view('welcome')->with($data);
    }
    public function about(){
        return view('front.about');
    }
    public function contact(){
        return view('front.contact');
    }
    public function jobView($id){
        $job=PostJob::find($id);
        if($job==null){
            return back();
        }  
        $relativeJobs=PostJob::where('category_id',$job->category->id)
                        ->orderBy('id','DESC')->get();
        // dd($relativeJobs);
        $data = [
            'job'=>$job,
            'relativeJobs'=>$relativeJobs
         ];
        return view('front.view')->with($data);
    }
    public function jobCategories(){
        $cats=JobCategories::all();
        // dd($cats);
        $data = [
           'categories'=>$cats
        ];
        return view("front.job-categories")->with($data);
    }
    public function allJobsByCategory($id)
    {
        // dd();
        $jobs=PostJob::where('category_id',$id)->orderBy('id','DESC')->paginate(10);
        $cats=JobCategories::find($id);
        if( $cats==null){
            return back();
        }
        // dd($cats);
        $data = [
           'jobs'=>$jobs,
           'category'=>$cats
        ];
        // dd();
        return view('front.jobs-by-category')->with($data);
    }
    public function autocomplete(Request $request)
    {
        $data = PostJob::select("job_title")->where("job_title","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }
    public function searchComplete(Request $request){
       if($request->has('name')){
           $article=PostJob::where('id',$request->id)->firstOrFail();
           $categories = Categories::all();
        //    return view("client.article-search",compact('article','categories'));
       }else{
           return back();
        }
    }
}
