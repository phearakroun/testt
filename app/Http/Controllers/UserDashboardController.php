<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use PostJob;
use JobCategories;
use companies;
class UserDashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){

        $user=Auth::user();
        // dd($user);
        // $company=companies::where('user_id',$user->id)->get();
        // $jobs=PostJob::where('user_id',$user->id)->get();
       
        $data=[
            'user'=>$user,
            // 'company'=>$company
        ];
        return view('front.user.index')->with($data);
    }
    public function company($userId){
        return view('front.company.index')->with();
    }
    public function jobs($userId){

        return view('')->with();
    }
}
