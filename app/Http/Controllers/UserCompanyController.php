<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\companies;
use App\Http\Requests\admin\ComapyRequest;
class UserCompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $companies=companies::where('user_id',$user->id )->where('active','1')->paginate(10);
        // dd($companies);
        $data=[
            'companies'=>$companies
        ];
        return view('front.user.company.index')->with($data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front.user.company.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComapyRequest $request)
    {
        $path=$request->file('company_logo')->store('companies');
        $com = new companies();
        $com->user_id=Auth::user()->id;
        $com->company_name=$request->company_name;
        $com->company_email=$request->company_email;
        $com->company_phone_number=$request->company_phone_number;
        $com->company_website_url=$request->company_website_url;
        $com->company_address=$request->company_address;
        $com->company_description=$request->company_description;
        $com->company_logo=$path;
        $com->save();
        return redirect()->route('user.company');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coms=companies::find($id);
        $data=array("com"=>$coms);
        return view('front.user.company.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'company_name' => 'required|string|max:255',
            'company_phone_number' => 'required|string|max:255',
            'company_email' => 'required|email|max:255',
            'company_address' => 'required|string',
           

        ]);


        $compa = companies::find($id);

        if($request->hasFile('company_logo')){
            $path=$request->file('company_logo')->store('companies');
        }
        if($request->company_logo==""){
            $path=$compa->company_logo;
        };
        
        $compa->user_id=Auth::user()->id;
        $compa->company_name=$request->company_name;
        $compa->company_email=$request->company_email;
        $compa->company_phone_number=$request->company_phone_number;
        $compa->company_website_url=$request->company_website_url;
        $compa->company_address=$request->company_address;
        $compa->company_description=$request->company_description;
        $compa->company_logo=$path;
        $compa->save();
        return redirect()->route('user.company');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $data=companies::find($request->id);
        $data->status=0;
        $data->save();
        return "Deleted";
    }
}
