<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PostJob;
use App\companies;
use App\JobCategories;
use App\JobLocation;
use App\JobSchedule;
use Auth;
use App\Http\Requests\admin\PostJobRequest;
class UserJobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $jobs = DB::table('post_jobs')
                    ->join('job_locations', 'post_jobs.location_id', '=', 'job_locations.id') 
                    ->join('companies', 'post_jobs.company_id', '=', 'companies.id')   
                    ->join('users', 'companies.user_id', '=', 'users.id')     
                    ->select('post_jobs.id','post_jobs.job_title',
                        'companies.company_name',
                        'job_locations.joblocation',
                        'post_jobs.close_date',
                        'post_jobs.status')
                        ->where('users.status','1')
                        ->where('users.id',$user->id)
                        ->orderBy('post_jobs.id', 'desc')
                        ->paginate(10);
        $data=[
                'jobs'=>$jobs
            ];
        return view('front.user.jobs.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=JobCategories::all();
        $companies=companies::where('user_id',Auth::user()->id)->where('status','Approved')->get();
        $locations=JobLocation::all();
        $schedules=JobSchedule::all();
        $data=[
            'categories'=>$categories,
            'locations'=>$locations,
            'schedules'=>$schedules,
            'companies'=>$companies
        ];
        return view('front.user.jobs.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostJobRequest $request)
    {
       
        try{
                $job=new PostJob();
                $job->job_title=$request->title;
                $job->post_by=Auth::user()->id;
                $job->company_id=$request->company;
                $job->category_id=$request->category;
                $job->location_id=$request->location;
                $job->schedule_id=$request->schedule;
                $job->requirements=$request->requirements;
                $job->duties=$request->duties;
                $job->salary=$request->salary;
                $job->post_date=date('Y-m-d H:i:s');
                $job->close_date=date('Y-m-d H:i:s', strtotime($request->close_date)) ;
                $job->save();
                $data=[
                    "message"=>true
                ];
               
                return redirect()->route('user.job.index');
            }catch(\Exception $e){
                $data=[
                    "message"=>false
                ];
                return back()->with($data);
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job=PostJob::find($id);
        if($job==null){
            return back();
        }
        $categories=JobCategories::all();
        $companies=companies::where('user_id',Auth::user()->id)->get();
        $locations=JobLocation::all();
        $schedules=JobSchedule::all();
        $data=[
            'categories'=>$categories,
            'locations'=>$locations,
            'schedules'=>$schedules,
            'companies'=>$companies,
            'job'=>$job
        ];
        return view('front.user.jobs.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job=PostJob::find($id);
        if($job==null){
            return back();
        }
        try{
            
            $job->job_title=$request->title;
            $job->post_by=Auth::user()->id;
            $job->company_id=$request->company;
            $job->category_id=$request->category;
            $job->location_id=$request->location;
            $job->schedule_id=$request->schedule;
            $job->requirements=$request->requirements;
            $job->duties=$request->duties;
            $job->salary=$request->salary;
            // $job->post_date=date('Y-m-d H:i:s');
            $job->close_date=date('Y-m-d H:i:s', strtotime($request->close_date)) ;
            $job->save();
            $data=[
                "message"=>true
            ];
           
            return redirect()->route('user.job.index');
        }catch(\Exception $e){
            $data=[
                "message"=>false
            ];
            return back()->with($data);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $job=PostJob::find($request->id);
        $job->delete();
        return "Deleted";
    }
}
