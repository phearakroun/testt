<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use App\companies;
class ComapyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    
        return [


            'company_name' => 'required|string|max:255|unique:companies,company_name',
            'company_phone_number' => 'required|string|max:255|unique:companies,company_phone_number',
            'company_email' => 'required|email|max:255|unique:companies,company_email',
            'company_address' => 'required|string',
            'company_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',            
        ];
    }
}
