<?php

namespace App\Http\Requests\admin;


use Illuminate\Foundation\Http\FormRequest;

class JobCategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jobcategoriesname' => 'required|string|max:255|unique:job_categories,jobcategoriesname',
        ];
    }
}
