<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class JobLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'joblocation' => 'required|string|max:255|unique:job_locations,joblocation',
        ];
    }
}
