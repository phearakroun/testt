<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobLocation extends Model
{
    //
    public function jobs(){
    	return $this->hasMany(PostJob::class,'location_id','id');
    }
}
