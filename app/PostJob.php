<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJob extends Model
{

    // public $fillable = ['id','title'];
	public function getLocation(){
		return JobLocation::where('id',$this->location_id)->first()->name;
	}

    public function location(){
    	return $this->beLongsTo(JobLocation::class,'location_id','id');
    }
    public function job(){
        return $this->beLongsTo(JobCategories::class,'category_id','id');
    }
    public function schedule(){
    	return $this->beLongsTo(JobSchedule::class,'schedule_id','id');
    }
    public function company(){
    	return $this->beLongsTo(companies::class,'company_id','id');
    }
    public function category(){
    	return $this->beLongsTo(JobCategories::class,'category_id','id');
    }

}
