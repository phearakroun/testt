@extends('layouts.app')
@section('title','Apply Job - ')
@section('content')
<br>
<div class="row">
  <div class="col-md-2">  
  </div>
  <div class="col-md-8">

          <div class="card">
            <div class="card-header">
              <span class="card-title">Apply Job</span>
              {{-- <a href="{{route('user.clent.apply.job.submit')}}" class="btn btn-primary">ss</a> --}}
            </div>
           
            <div class="card-body">
          
              <form action="{{route('user.clent.apply.job.submit')}}" method="post" enctype="multipart/form-data">
                @csrf

              <input type="text" name="job_id" value="{{$data}}" hidden>
        
          <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
            <label for="phone">Phone:</label>
            <input type="number" class="form-control" id="text" placeholder="phone" name="phone" value="{{old('phone')}}">

            @if($errors->has('phone'))
            <span class="has-error">
              {{ $errors->first('phone') }}
            </span>
            @endif
          </div>

          <div class="form-group {{ $errors->has('document') ? 'has-error' : ''}}">
            <label for="document">CV:</label>
            <input type="file" class="form-control" id="text" name="document">

            @if($errors->has('document'))
            <span class="has-error">
              {{ $errors->first('document') }}
            </span>
            @endif

          </div>



          <button type="submit" class="btn btn-primary float-right">Save</button>
      
        </form>
      </div>
    </div>   
  </div>
  <div class="col-md-2">     
  </div>
</div>
    
     
<br>
  @endsection