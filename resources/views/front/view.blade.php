@extends('layouts.app')

@section('content')
<div class="site-section ">
    <div class="container">
      <div class="row rounded border jobs-wrap">
        <div class="col-md-12 col-lg-8 mb-5">
          <div class="p-5 bg-white">
            <div class="mb-4 mb-md-5 mr-5">
             <div class="job-post-item-header d-flex align-items-center">
             <h2 class="mr-3 text-black h4">{{$job->job_title}}</h2>
               <div class="badge-wrap">
                 {{-- <a href="{{route('jobs-categories-id',['id'=>$job->category->id])}}"> --}}
               {{-- <span class="border border-warning text-warning py-2 px-4 rounded">{{$job->category->jobcategoriesname}}</span> --}}
              {{-- </a> --}}
              </div>
             </div>
             <div class="job-post-item-body d-block d-md-flex">
               <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span> <a href="#">Location : {{$job->location->joblocation}} / </a></div>
             <div><span class="fl-bigmug-line-big104"></span> <span> Schedule : {{$job->schedule->schedule_name}} /   </span></div>  
             <div><span class="fl-bigmug-line-big104"></span> <span>  Dateline : {{$job->close_date}}</span></div>
            
            </div>
            </div>
            
            <div class="img-border mb-5">
            <a href="{{url('fron/img-logo/logo.png')}}" class="popup-vimeo image-play">
                <span class="icon-wrap">
                  <span class="icon icon-play"></span>
                </span>
                <img src="{{url('fron/img-logo/logo.png')}}" alt="Image" class="img-fluid rounded">
              </a>
            </div>
            <h4>Requirements</h4>
           <p>
             {{$job->requirements}}
           </p>

           <h4>Duties</h4>
           <p>
             {{$job->duties}}
           </p>
          <p class="mt-5"><a href="{{route('user.cleint.apply.job',$job->id)}}" class="btn btn-primary  py-2 px-4">Apply Job</a></p>
          </div>
        </div>

        <div class="col-lg-4">
          
          <div class="p-4 mb-3 bg-white">
            <h3 class="h5 text-black mb-3">Company Information</h3>
            {{-- <p>Company Name : </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dolore, ipsa consectetur</p>
            
            <hr> --}}
          <p><span>Company Name : </span> {{$job->company->company_name}}</p>
          <p><span>E-mail : </span> {{$job->company->company_email}}</p>
          <p><span>Website : </span> {{$job->company->company_website_url}}</p>
          <p><span>Phone : </span> {{$job->company->company_phone_number}}</p>
          {{-- <p><a href="#" class="btn btn-primary  py-2 px-4">Apply Job</a></p> --}}
          </div>
          {{-- $table->integer('user_id')->nullable();
          $table->string('company_name')->unique();
          $table->string('company_email')->unique();
          $table->string('company_phone_number')->unique();
          $table->string('company_website_url')->nullable();
          $table->text('company_description')->nullable();
          $table->text('company_address');
          $table->string('company_logo'); --}}
        </div>
      </div>
    </div>
  </div>
  {{--  --}}
  <div class="row">
    <br>
    <div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
      <h4>Relative Jobs</h4>
      <hr>
      <div class=" jobs-wrap">
        <div class="row">
          @foreach ( $relativeJobs as $job)
          <div class="col-md-6">
          <a style="border-left: 3px solid #26baee;
          border-right: 3px solid #eaeaea;
          padding-top: 10px;
          margin-top: 10px;" href="{{route('view',['id'=>$job->id])}}" class="job-item d-block d-md-flex align-items-center  border-bottom fulltime">
              <div class="company-logo blank-logo text-center text-md-left pl-3">
                <img src="{{url('front/img-logo/logo.png')}}" alt="Image" class="img-fluid mx-auto">
              </div>
              <div class="job-details h-100">
                <div class="p-3 align-self-center">
                  <h3>{{$job->job_title}}</h3>
                  <div class="d-block d-lg-flex">
                  <div class="mr-3"><span class="icon-suitcase mr-1">
                    </span>{{$job->location->joblocation}}</div>
                    <div class="mr-3"><span class="icon-room mr-1"></span> Salary : </div>
                    <div><span class="icon-money mr-1">$ {{floatval($job->salary)}} </span></div>
                  </div>
                </div>
              </div>
              <div class="job-category align-self-center">
                <div class="p-3">
                <span class="text-info p-2 rounded border border-info">{{$job->schedule->schedule_name}}</span>
                </div>
              </div>
            </a>
          </div>
          @endforeach
  
  
        </div>
      </div>
      <div class="col-md-12 text-center mt-5">
        {{-- {{ $jobs->links() }} --}}
        <a href="/jobs" class="btn btn-primary rounded py-3 px-5"><span class="icon-plus-circle"></span> Show More Jobs</a>
      </div>
    </div>
  
  </div>
<br>
@endsection