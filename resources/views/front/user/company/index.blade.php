@extends('layouts.app')
@section('title','All Comapies - ')
@section('content')
<br>
<div class="col-md-12 mb-5 mb-md-0">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h4>
                  All Company
                <a href="{{route('user.create.company')}}" class="btn btn-primary float-right">New</a>

                </h4>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Company Name</th>
                    <th>Phone Number</th>
                    <th>Logo</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($companies as $val)
                  <tr>
                    <th>{{ $val->id}}</th>
                    <td>{{$val->company_name}}</td>
                    <td>{{$val->company_phone_number}}</td>
                    <td><img src="{{url('storage',$val->company_logo)}}" height="50"></td>
                    <td>

                      @if ($val->status == 'Approved')
                      <span class="bg-primary text-white p-1">{{$val->status}}</span>
                  @else
                  
                   
                             
                            
                              <span class="bg-success text-white p-1" >
                                {{$val->status}}
                              </span>
                  
                  
                  @endif


                    </td>
                    <td>
                      <a href="{{route('user.edit.company',$val->id)}}" class="btn btn-primary btn-sm" >
                        Edit
                      </a>
                      <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
                       Delete
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="pull-right">
                {{$companies->links()}}
              </div>
        </div>
    </div>
    
   
</div>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');
          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('user.delete.company')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>
<br>


@endsection