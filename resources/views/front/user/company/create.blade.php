@extends('layouts.app')
@section('title','Add new Company - ')
@section('content')
<br>
<div class="card">

  <div class="card-header">
    <span class="card-title">Create new Company</span>
    <a href="{{route('user.company')}}" class="btn btn-info float-right">Back</a>
  </div>
 
  <div class="card-body">

    <form action="{{route('user.submit.company')}}" method="post" enctype="multipart/form-data">
      {{ csrf_field()}}
      <div class="row">
        <div class="col-md-6">

          <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
            <label for="company_name">Company Title</label>
            <input type="text" class="form-control" placeholder="company name" name="company_name" value="{{old('company_name')}}" >
            @if($errors->has('company_name'))
            <span class="has-error">
              {{ $errors->first('company_name') }}
            </span>
            @endif
          </div>
          <div class="form-group {{ $errors->has('company_email') ? 'has-error' : ''}}">
            <label for="company_email">Email:</label>
            <input type="email" class="form-control" id="text" placeholder="company email" name="company_email" value="{{old('company_email')}}">

            @if($errors->has('company_email'))
            <span class="has-error">
              {{ $errors->first('company_email') }}
            </span>
            @endif


          </div>
          <div class="form-group {{ $errors->has('company_phone_number') ? 'has-error' : ''}}">
            <label for="company_phone_number">Contact Phone:</label>
            <input type="number" class="form-control" id="text" placeholder="phone number" name="company_phone_number" value="{{old('company_phone_number')}}">

            @if($errors->has('company_phone_number'))
            <span class="has-error">
              {{ $errors->first('company_phone_number') }}
            </span>
            @endif
          </div>
          <div class="form-group">
            <label for="company_website_url">Website:</label>
            <input type="text" class="form-control" id="text" placeholder="www.example.com" name="company_website_url" value="{{old('company_website_url')}}">
          </div>
          


        </div>
        <div class="col-md-6">
          
          <div class="form-group {{ $errors->has('company_logo') ? 'has-error' : ''}}">
            <label for="company_logo">Logo:</label>
            <input type="file" class="form-control" id="text" name="company_logo">

            @if($errors->has('company_logo'))
            <span class="has-error">
              {{ $errors->first('company_logo') }}
            </span>
            @endif

          </div>
          <div class="form-group {{ $errors->has('company_address') ? 'has-error' : ''}}">
            <label for="company_address">Address:</label>
            <textarea type="text" class="form-control" placeholder="company address" name="company_address" rows="3">{{old('company_address')}}</textarea>

            @if($errors->has('company_address'))
            <span class="has-error">
              {{ $errors->first('company_address') }}
            </span>
            @endif

          </div>
          <div class="form-group">
            <label for="company_description">Description:</label>
             <textarea type="text" class="form-control" placeholder="description" name="company_description" rows="3">{{old('company_description')}}</textarea>
          </div>
          
        </div>
      </div>
      
      
      <button type="submit" class="btn btn-primary float-right">Save</button>
      
    </form>
  </div>
</div>
<br>
  @endsection