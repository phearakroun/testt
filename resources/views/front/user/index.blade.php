@extends('layouts.app')
@section('content')
<br>

<div class="card">
    <div class="card-header">
      <span class="card-title">{{$user->name}} - Profile</span>
      {{-- <a href="{{route('user.clent.apply.job.submit')}}" class="btn btn-primary">ss</a> --}}
    </div>
   
    <div class="card-body">

 

<div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
    <div class="row">
        <div class="col-sm-10">
        <h1>{{$user->name}}</h1>
        </div>
        {{-- <div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image"
                    class="img-circle img-responsive"
                    src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div> --}}
    </div>
    <div class="row">
        <div class="col-sm-3">
            <!--left col-->


            <div class="text-center">
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail"
                    alt="avatar">
                <h6>Upload a different photo...</h6>
                <input type="file" class="text-center center-block file-upload">
            </div>
            </hr><br>


            <div class="panel panel-default">
                <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i></div>
                <div class="panel-body"><a href="http://bootnipets.com">bootnipets.com</a></div>
            </div>


        

            {{-- <div class="panel panel-default">
                <div class="panel-heading">Social Media</div>
                <div class="panel-body">
                    <i class="fa fa-facebook fa-2x"></i> <i class="fa fa-github fa-2x"></i> <i
                        class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i
                        class="fa fa-google-plus fa-2x"></i>
                </div>
            </div> --}}

        </div>
        <!--/col-3-->
        <div class="col-sm-9">
            <h4 class="nav nav-tabs">
              Detail Information

            </h2>
            <br>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                    <form class="form" action="##" method="post" id="registrationForm">
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="first_name">
                                    <h4>User Name</h4>
                                </label>
                                <input type="text" class="form-control" name="first_name" id="first_name"
                            placeholder="first name" title="enter your first name if any." value="{{$user->name}}">
                            </div>
                        </div>
                    
                        {{-- <div class="form-group">

                            <div class="col-xs-6">
                                <label for="phone">
                                    <h4>Phone</h4>
                                </label>
                                <input type="text" class="form-control" name="phone" id="phone"
                                    placeholder="enter phone" title="enter your phone number if any.">
                            </div>
                        </div> --}}

                        {{-- <div class="form-group">
                            <div class="col-xs-6">
                                <label for="mobile">
                                    <h4>Mobile</h4>
                                </label>
                                <input type="text" class="form-control" name="mobile" id="mobile"
                                    placeholder="enter mobile number" title="enter your mobile number if any.">
                            </div>
                        </div> --}}
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email">
                                    <h4>Email</h4>
                                </label>
                                <input type="email" class="form-control" name="email" id="email"
                            placeholder="you@email.com" title="enter your email." value="{{$user->email}}">
                            </div>
                        </div>
                        {{-- <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email">
                                    <h4>Location</h4>
                                </label>
                                <input type="email" class="form-control" id="location" placeholder="somewhere"
                                    title="enter a location">
                            </div>
                        </div> --}}
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="password">
                                    <h4>Password</h4>
                                </label>
                                <input type="password" class="form-control" name="password" id="password"
                            placeholder="password" title="enter your password.">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="password2">
                                    <h4>Verify</h4>
                                </label>
                                <input type="password" class="form-control" name="password2" id="password2"
                                    placeholder="password2" title="enter your password2.">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit"><i
                                        class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i>
                                    Reset</button>
                            </div>
                        </div>
                    </form>

                    <hr>

                </div>
               
            </div>
            <!--/tab-pane-->
        </div>
        <!--/tab-content-->

    </div>
    <!--/col-9-->
</div>

</div>
</div>
<br>
<br>

@endsection
