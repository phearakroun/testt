@extends('layouts.app')
@section('content')
<br>


<div class="col-md-12 mb-5 mb-md-0">

  <div class="card">
    <div class="card-header">Create Job</div>
    <div class="card-body">
      <form action="{{route('user.job.store')}}" method="post">
        @csrf

        <div class="row">
          <div class="col-md-6">

            
        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
          <label for="">Job Title</label>
          <input type="text" class="form-control" placeholder="Enter Job Title" name="title" value="{{old('title')}}">
          @if($errors->has('title'))
          <span class="has-error">
            {{ $errors->first('title') }}
          </span>
          @endif
        </div>
        <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
          <label for="">Select Company</label>
          <select type="text" name="company" class="form-control">
            @foreach ($companies as $com)
            <option value="{{$com->id}}">{{$com->company_name}}</option>
            @endforeach
          </select>
          @if($errors->has('company'))
          <span class="has-error">
            {{ $errors->first('company') }}
          </span>
          @endif
        </div>
        
        <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
          <label for="">Select Categories</label>
          <select type="text" class="form-control" name="category">
            @foreach ($categories as $com)
            <option value="{{$com->id}}">{{$com->jobcategoriesname}}</option>
            @endforeach
          </select>
          @if($errors->has('category'))
          <span class="has-error">
            {{ $errors->first('category') }}
          </span>
          @endif
        </div>
        <div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
          <label for="">Select Locaiton</label>
          <select type="text" class="form-control" name="location" >
            @foreach ($locations as $com)
            <option value="{{$com->id}}">{{$com->joblocation}}</option>
            @endforeach
          </select>
          @if($errors->has('location'))
          <span class="has-error">
            {{ $errors->first('location') }}
          </span>
          @endif
        </div>

          </div>
          <div class="col-md-6 {{ $errors->has('schedule') ? 'has-error' : ''}}">
            <div class="form-group">
              <label for="">Select Schedule</label>
              <select type="text" class="form-control" name="schedule">
                @foreach ($schedules as $com)
                <option value="{{$com->id}}">{{$com->schedule_name}}</option>
                @endforeach
              </select>
              @if($errors->has('schedule'))
              <span class="has-error">
                {{ $errors->first('schedule') }}
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('salary') ? 'has-error' : ''}}">
              <label for="">Salary $ </label>
              <input type="number" name="salary" class="form-control" placeholder="200.00 $" value="0">
              @if($errors->has('salary'))
              <span class="has-error">
                {{ $errors->first('salary') }}
              </span>
              @endif
            </div>

            <div class="form-group {{ $errors->has('close_date') ? 'has-error' : ''}}">
              <label for="">Dateline</label>
              <input type="text" class="form-control" id="txtdate" name="close_date" placeholder="Please Dateline...">
                @if($errors->has('close_date'))
                <span class="has-error">
                  {{ $errors->first('close_date') }}
                </span>
                @endif
            </div>

            
          </div>
         
          
        </div>

        <div class="form-group {{ $errors->has('duties') ? 'has-error' : ''}}">
          <label for="">Duties</label>
          <textarea name="duties" class="form-control" id="" cols="30" rows="5">{{old('duties')}}</textarea>
          @if($errors->has('duties'))
          <span class="has-error">
            {{ $errors->first('duties') }}
          </span>
          @endif
        </div>
        <div class="form-group {{ $errors->has('requirements') ? 'has-error' : ''}}">
          <label for="">Requirements</label>
        <textarea name="requirements" class="form-control" id="" cols="30" rows="5" >{{old('requirements')}}</textarea>
          @if($errors->has('requirements'))
          <span class="has-error">
            {{ $errors->first('requirements') }}
          </span>
          @endif
        </div>


        <div class="form-group">
          <input type="submit" value="Post a Job" class="btn btn-primary float-right" >
        </div>
      </form>

    </div>
    
  </div>



</div>

<br>
<br>

@endsection