@extends('layouts.app')
@section('content')
<br>

<div class="col-md-12 mb-5 mb-md-0">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                All Jobs
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Job Title</th>
                    <th>Company</th>
                    <th>Location</th>
                    <th>Close Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
    
                  @foreach($jobs as $val)
                  <tr>
                    <th>{{$val->id}}</th>
                    <td>{{$val->job_title}}</td>
                    <td>{{$val->company_name}}</td>
                    <td>
                      {{$val->joblocation}}
                    </td>
                    <td>{{$val->close_date}}</td>
                    <td>
                   
                          
                  <a href="{{route('user.job.edit',$val->id)}}" class="btn btn-primary btn-sm" >
                        <i class="glyphicon glyphicon-pencil">Edit</i>
                      </a>
  
                      <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
                            <i class="glyphicon glyphicon-trash">Delete</i>
                        </a>
                    </td>
                 @endforeach
                </tbody>
              </table>
              <div class="pull-right">
                {{$jobs->links()}}
              </div>
        </div>
    </div>
</div>
<br>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');
          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('user.job.delete')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>

@endsection