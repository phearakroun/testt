@extends('layouts.app')
@section('content')
<br>


<div class="col-md-12 mb-5 mb-md-0">

  <div class="card">
    <div class="card-header"><h4>
      Edit Job
      <a href="{{route('user.company')}}" class="btn btn-primary float-right">Back</a>
    </h4></div>
    <div class="card-body">
      <form action="{{route('user.job.update',['id'=>$job->id])}}" method="post">
        @csrf

        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label for="">Job Title</label>
              <input type="text" class="form-control" placeholder="Enter Job Title" name="title" required value="{{$job->job_title}}">
              @if($errors->has('title'))
              <span class="has-error">
                {{ $errors->first('title') }}
              </span>
              @endif
            </div>

           <div class="form-group">
            <label for="">Select Company</label>
            <select type="text" name="company" class="form-control">
              @foreach ($companies as $com)
              {{-- <option value="{{$val->id}}" {{$postjob->category_id == $val->id  ? 'selected' : ''}}>{{$val->jobcategoriesname}}</option> --}}
  
                <option value="{{$com->id}} "  {{$job->company_id == $com->id  ? 'selected' : ''}}>{{$com->company_name}}</option>
              @endforeach
            </select>
            @if($errors->has('company'))
            <span class="has-error">
              {{ $errors->first('company') }}
            </span>
            @endif
          </div>
          <div class="form-group">
            <label for="">Select Categories</label>
            <select type="text" class="form-control" name="category">
              @foreach ($categories as $com)
              <option value="{{$com->id}}" {{$job->category_id == $com->id  ? 'selected' : ''}} >{{$com->jobcategoriesname}}</option>
              @endforeach
            </select>
            @if($errors->has('category'))
            <span class="has-error">
              {{ $errors->first('category') }}
            </span>
            @endif
          </div>
          <div class="form-group">
            <label for="">Select Locaiton</label>
            <select type="text" class="form-control" name="location" >
              @foreach ($locations as $com)
              <option value="{{$com->id}}" {{$job->location_id == $com->id  ? 'selected' : ''}}>{{$com->joblocation}}</option>
              @endforeach
            </select>
            @if($errors->has('location'))
            <span class="has-error">
              {{ $errors->first('location') }}
            </span>
            @endif
          </div>
 
        

          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Select Schedule</label>
              <select type="text" class="form-control" name="schedule">
                @foreach ($schedules as $com)
                <option value="{{$com->id}}" {{$job->schedule_id == $com->id  ? 'selected' : ''}}>{{$com->schedule_name}}</option>
                @endforeach
              </select>
              @if($errors->has('schedule'))
              <span class="has-error">
                {{ $errors->first('schedule') }}
              </span>
              @endif
            </div> 


            <div class="form-group">
              <label for="">Salary $ </label>
            <input type="number" value="{{floatval($job->salary)}}" name="salary" class="form-control" placeholder="200.00 $" value="0" required>
              @if($errors->has('salary'))
              <span class="has-error">
                {{ $errors->first('salary') }}
              </span>
              @endif
            </div>
            <div class="form-group">
              <label for="">Dateline</label>
            <input value="{{$job->close_date}}" type="text" class="form-control" id="txtdate" name="close_date" placeholder="Please Dateline..."
                required>
                @if($errors->has('close_date'))
                <span class="has-error">
                  {{ $errors->first('close_date') }}
                </span>
                @endif
            </div>

            
          </div>
         
          
        </div>

        <div class="form-group">
          <label for="">Duties</label>
          <textarea  name="duties" class="form-control" id="" cols="30" rows="5" required >
            {{$job->duties}}
          </textarea>
          @if($errors->has('duties'))
          <span class="has-error">
            {{ $errors->first('duties') }}
          </span>
          @endif
        </div>
        <div class="form-group">
          <label for="">Requirements</label>
          <textarea name="requirements" class="form-control" id="" cols="30" rows="5" required>
            {{$job->requirements}}
          </textarea>
          @if($errors->has('requirements'))
          <span class="has-error">
            {{ $errors->first('requirements') }}
          </span>
          @endif
        </div>


        <div class="form-group">
          <input type="submit" value="Update Job" class="btn btn-primary float-right" >
        </div>
      </form>

    </div>
    
  </div>



</div>

<br>

@endsection