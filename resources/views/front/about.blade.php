@extends('layouts.app')
@section('content')
<br>

<div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
    {{-- <h4>All Jobs By :: {{$category->jobcategoriesname}}</h4> --}}
    <div class="row">
        <h3 class="ml-4">
            About
        </h3>
        <p style="color: rgb(36, 76, 161)!important;" class="pl-4"><strong style="color: red">The Gate enterprise co,.ltd</strong>  is a social enterprise is firstly established with its main purpose is to reduce the poverty of Cambodian people and youth unemployment through leveraging more job opportunity for unskilled/skilled workers, technicians and graduated student in Cambodia. Regarding to the national and regional market demand and economic growth. Due to the lack of interest of local and international investors in investing in skill development program, lack of soft skill of skilled workers, job readiness, lack of connectors between unskilled/skilled workers, technician, fresh graduated student, and employers across the country as well as the employers are also lacking of source to search for the qualified employee, technician and unskilled/skilled workers to supply the business’s growth recently. So, it is an initiative and innovative for connecting between employers and job seekers in responding to the business growth in Cambodia. To response to facing demand of human capital view, The Gate for Cambodia is playing an important role in providing the reliable service in recruiting the right employee, unskilled/skilled workers and technician in Cambodia to fill out the business demand and the sustainability development goal contribution “Good Job, No Poverty and Economic Growth” in Cambodia and globally.</p>
    </div>
    <div class="row">
        <p style="color: rgb(36, 76, 161)!important;" class="ml-4"><strong style="color: red">The Gate enterprise co,.ltd</strong>  is a social enterprise is firstly established with its main purpose is to reduce the poverty of Cambodian people.
        </p>
    </div>
    <div class="row">
        <h3 class="ml-4">Company Overview</h3>
        <p style="color: rgb(36, 76, 161)!important;" class="pl-4">
            <strong style="color: red">The Gate enterprise co,.ltd</strong>
            is a social enterprise is firstly established with its main purpose is to reduce the poverty of Cambodian people and youth unemployment through leveraging more job opportunity for unskilled/skilled workers, technicians and graduated student in Cambodia. Regarding to the national and regional market demand and economic growth, The Gate for Cambodia is initiated by Mr. Phan Chanvandy based on over 10-year experience in working with Cambodian youth in skill and employment development program both local and international organization would be a leading and good partnership with reliable and professional service aspect. He has learnt that due to the lack of interest of local and international investors in investing in skill development program, lack of soft skill of skilled workers, job readiness, lack of connectors between unskilled/skilled workers, technician, fresh graduated student, and employers across the country as well as the employers are also lacking of source to search for the qualified employee, technician and unskilled/skilled workers to supply the business’s growth recently. So, it is an initiative and innovative for connecting between employers and job seekers in responding to the business growth in Cambodia. To response to facing demand of human capital view, The Gate for Cambodia is playing an important role in providing the reliable service in recruiting the right employee, unskilled/skilled workers and technician in Cambodia to fill out the business demand and the sustainability development goal contribution “Good Job, No Poverty and Economic Growth” in Cambodia and globally.
        </p>

    </div>
    <div class="row">
        <h3 class="ml-4">Product</h3>
        <ol class="blue-color">
            <li>Job Posting Service</li>
            <li>Internship & Employment Placement Service</li>
            <li> Skill Development and Scholarship Program</li>
            <li> Research and Advocacy Program</li>
        </ol>

    </div>
    <br>
    <br>
</div>

@endsection