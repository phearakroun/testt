@extends('layouts.app')
@section('content')
<br>
<br><br>
<div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
    {{-- <h4>All Jobs By :: {{$category->jobcategoriesname}}</h4> --}}
    <div class="row">
        <h3><i class="fa fa-connectdevelop"></i> Customer Service team</h3>
        <hr class="mt-0">
        <p class="red-color"><b class="red-color">Hotline :</b> 023 456 789  (/ENG/)</p>
        <p class="red-color">011 55 11 46 / 071 65 77 61 / 017 123 4567</p>


        <p class="p-content">
            Job Post Service<br>
            Phone Number :<br><br>
            Job Post Email: jobgetenterprise.com.kh<br>
            Customer Care Email: csgetenterprise.com.kh<br>
            Apply Job Email: cvgetenterprise.com.kh<br>
            Our address: 1st fisrt floor #335 , street,1987, Phnom Penh thmey, Sensok, Phnom Penh, Cambodia


        </p>
    </div>
    <div class="row  mt-5">
        <h3 class="marketing-team"><i class="fa fa-headphones"></i> Officail Marketing Team</h3>
        <hr class="mt-0">
        <div class="row">
            <div class="col-sm-6">
                <p class="p-content">
                    Mr.Kim Srieng<br>
                    Senior Webdeveloper<br>
                    Tel : 015 55 11 46 / 081 65 77 61<br>
                    E-mail: srieng@gmail.com

                </p>

            </div>
            <div class="col-sm-6">
                <p class="p-content">
                    Mr.Kim Srieng<br>
                    Senior Webdeveloper<br>
                    Tel : 015 55 11 46 / 081 65 77 61<br>
                    E-mail: srieng@gmail.com





                </p>

            </div>
        </div>
    </div>
    <div class="row  mt-5">
        <h3 class="inter-map"><i class="fa fa-globe"></i>The Grade Interprise Map</h3>
        <hr class="mt-0">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.226670046595!2d104.92250401412245!3d11.535591147877478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950da4db34301%3A0x61232833a62cac36!2z4Z6f4Z624Z6A4Z6b4Z6c4Z634Z6R4Z-S4Z6Z4Z624Z6b4Z-Q4Z6Z4oCL4Z6X4Z684Z6Y4Z634Z6T4Z-S4Z6R4Z6T4Z644Z6P4Z634Z6f4Z624Z6f4Z-S4Z6P4Z-S4Z6aIOGek-Get-GehOGenOGet-GekeGfkuGemeGetuGen-GetuGen-GfkuGej-GfkuGemuGen-GfgeGeiuGfkuGei-GegOGet-GeheGfkuGehQ!5e0!3m2!1skm!2skh!4v1563899558445!5m2!1skm!2skh" width="100%" height="500"  frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <br><br><br>
</div>

@endsection