@extends('layouts.app')
@section('content')
<br>
<br>
<br>
<div class="row">
  <div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
  <h4>All Jobs By :: {{$category->jobcategoriesname}}</h4>
    <hr>
    <div class=" jobs-wrap">
      <div class="row">
        @foreach ( $jobs as $job)
        <div class="col-md-6">
        <a style="border-left: 3px solid #26baee;
        border-right: 3px solid #eaeaea;
        padding-top: 10px;
        margin-top: 10px;" href="{{route('view',['id'=>$job->id])}}" class="job-item d-block d-md-flex align-items-center  border-bottom fulltime">
            <div class="company-logo blank-logo text-center text-md-left pl-3">
              <img src="{{url('front/img-logo/logo.png')}}" alt="Image" class="img-fluid mx-auto">
            </div>
            <div class="job-details h-100">
              <div class="p-3 align-self-center">
                <h3>{{$job->job_title}}</h3>
                <div class="d-block d-lg-flex">
                <div class="mr-3"><span class="icon-suitcase mr-1">
                  </span>{{$job->location->joblocation}}</div>
                  <div class="mr-3"><span class="icon-room mr-1"></span> Salary : </div>
                  <div><span class="icon-money mr-1">$ {{floatval($job->salary)}}</span> </div>
                </div>
              </div>
            </div>
            <div class="job-category align-self-center">
              <div class="p-3">
              <span class="text-info p-2 rounded border border-info">{{$job->schedule->schedule_name}}</span>
              </div>
            </div>
          </a>
        </div>
        @endforeach


      </div>
    </div>
    <div class="col-md-12 text-center mt-5">
      {{ $jobs->links() }}
      {{-- <a href="#" class="btn btn-primary rounded py-3 px-5"><span class="icon-plus-circle"></span> Show More Jobs</a> --}}
    </div>
  </div>

</div>

@endsection