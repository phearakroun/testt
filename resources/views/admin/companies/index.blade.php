@extends('Admin.master')
@section('title','All Companies - ')
@section('content')
<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">All Company</h3>
    
  </div>
  <div class="box-body">
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Company Name</th>
          <th>Phone Number</th>
          <th>Logo</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $val)
        <tr>
          <th>{{ $val->id}}</th>
          <td>{{$val->company_name}}</td>
          <td>{{$val->company_phone_number}}</td>
          <td><img src="{{url('storage',$val->company_logo)}}" height="50"></td>
          <td>

            @if ($val->status == 'Approved')
    <span class="text-success text-white p-1">{{$val->status}}</span>
@else

  <form method="post" action="{{route('comapnies.approved',$val->id)}}">
            {{ csrf_field()}}
          
            <button type="submit" class="btn btn-info btn-sm" >
             Not aproved
            </button>

           </form>

@endif


          </td>
          <td>
            <a href="{{route('companies.edit',$val->id)}}" class="btn btn-primary btn-sm" >
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            
            <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="pull-right">
      {{$data->links()}}
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');
          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('companies.delete')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>
@endsection