@extends('Admin.master')
@section('title','Update Company - ')
@section('content')
<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Update Company</h3>
    <a href="{{route('companies.index')}}" class="btn btn-info pull-right">Back</a></div>
    <div class="panel-body">
      <form action="{{route('companies.update',$com->id)}}" method="post" enctype="multipart/form-data">
          
        
        {{ csrf_field()}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
              <label for="company_name">Company Title</label>
              <input type="text" class="form-control" name="company_name" value="{{$com->company_name}}" >
              @if($errors->has('company_name'))
              <span class="has-error">
                {{ $errors->first('company_name') }}
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('company_email') ? 'has-error' : ''}}">
              <label for="company_email">Email:</label>
              <input type="email" class="form-control" id="text" name="company_email" value="{{$com->company_email}}">
              @if($errors->has('company_email'))
              <span class="has-error">
                {{ $errors->first('company_email') }}
              </span>
              @endif
            </div>
            <div  class="form-group {{ $errors->has('company_phone_number') ? 'has-error' : ''}}">
              <label for="company_phone_number">Contact Phone:</label>
              <input type="number" class="form-control" id="text" name="company_phone_number" value="{{$com->company_phone_number}}">
              @if($errors->has('company_phone_number'))
              <span class="has-error">
                {{ $errors->first('company_phone_number') }}
              </span>
              @endif
            </div>
            
            <div class="form-group">
              <label for="company_website_url">Website:</label>
              <input type="text" class="form-control" id="text" name="company_website_url" value="{{$com->company_website_url}}">
            </div>
            
          </div>
          <div class="col-md-6">
            
            <div class="form-group">
              <label for="company_logo">Logo:</label>
              <input type="file" class="form-control" id="text" name="company_logo">
              
            </div>

            <div class="form-group {{ $errors->has('company_address') ? 'has-error' : ''}}">
              <label for="company_address">Address:</label>
              <textarea type="text" class="form-control" name="company_address" rows="3">{{$com->company_address}}</textarea>
              
              @if($errors->has('company_address'))
              <span class="has-error">
                {{ $errors->first('company_address') }}
              </span>
              @endif
            </div>

            <div class="form-group">
              <label for="company_description">Description:</label>
              <textarea type="text" class="form-control" name="company_description" rows="3">{{$com->company_description}}</textarea>
            </div>
            
          </div>
        </div>
        
        
        <button type="submit" class="btn btn-primary pull-right">Save</button>
        
      </form>
    </div>
  </div>
  @endsection