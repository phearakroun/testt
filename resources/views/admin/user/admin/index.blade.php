@extends('Admin.master')
@section('title','All User Admin - ')
@section('content')

	<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">All User Admin</h3>
  	<a href="{{route('user.admin.create')}}" class="btn btn-primary pull-right">New</a>
  </div>
  <div class="box-body">
  	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($admin as $val)
      <tr>
        <th>{{$val->id}}</th>
        <td>{{$val->name}}</td>
        <td>{{$val->email}}</td>
        <td>

          <a href="{{route('user.admin.edit',$val->id)}}" class="btn btn-primary btn-sm" >
            <i class="glyphicon glyphicon-pencil"></i>
          </a>


           <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');

          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('user.admin.delete')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                // console.log(response)
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>


@endsection