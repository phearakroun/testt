@extends('Admin.master')
@section('title','Add new User Admin - ')
@section('content')
	<div class="row">
		<div class="col-md-8">			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Create new User Admin</h3>
  	<a href="{{route('user.admin')}}" class="btn btn-info pull-right">Back</a></div>
     <div class="box-body">

      <form method="POST" action="{{route('user.admin.create.submit')}}">
        {{ csrf_field()}}

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name">Name</label>

      
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

  
                @if($errors->has('name'))
                <span class="has-error">
                    {{ $errors->first('name') }}
                </span>
            @endif
    
         
        </div>

        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email">Email</label>

          
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

         


                @if($errors->has('email'))
                <span class="has-error">
                    {{ $errors->first('email') }}
                </span>
            @endif
    
         
        </div>

        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password">Password</label>

          
                <input id="password" type="password" class="form-control" name="password">


                @if($errors->has('password'))
                <span class="has-error">
                    {{ $errors->first('password') }}
                </span>
            @endif
    
           
        </div>

        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
            <label for="password-confirm">Confirm Password</label>

         
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
           

                @if($errors->has('password_confirmation'))
                <span class="has-error">
                    {{ $errors->first('password_confirmation') }}
                </span>
            @endif

            

        </div>

        <div class="form-group pull-right">
           
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
          
        </div>
    </form>

  </div>

</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection