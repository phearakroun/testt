@extends('Admin.master')
@section('title','All User Client - ')
@section('content')

	<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">All User Client</h3>

  </div>
  <div class="box-body">
  	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Type</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $val)
      <tr>
        <th>{{$val->id}}</th>
        <td>{{$val->name}}</td>
        <td>{{$val->email}}</td>
        <td>
          @if ($val->type == 'client')

          <a href="" class="btnClient btn btn-primary btn-sm" data-id="{{$val->id}}">
            {{$val->type}}
          </a>
         
      @else

      <a href="" class="btnAdmin btn btn-info btn-sm" data-id="{{$val->id}}">
        {{$val->type}}
      </a>
    
      @endif

        </td>
        <td>
          

         

          

           <a href="" class="btnDelteUser btn btn-danger btn-sm" data-id="{{$val->id}}">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  
  </div>
</div>

<script>
  $(document).ready(function(){

    
    //Delete User Client=============================
    $(".btnDelteUser").click(function(e) {

e.preventDefault();
  var id = $(this).data('id');

    Swal.fire({
      title: 'Are you sure to delete ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
        $.ajax({
        type: 'post',
        url: "{{route('user.client.delete')}}",
        data: {'_token':'{{ csrf_token() }}','id':id},
        success: function(response)
        {
          // console.log(response)
            location.reload();
        }
  });
  }
})
});

    //Client=========================================
    $(".btnClient").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');

          Swal.fire({
            title: 'Are you sure to admin ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('user.client.update.client')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                // console.log(response)
                  location.reload();
              }
        });
        }
      })
    });


    //Admin=============================
    $(".btnAdmin").click(function(e) {

e.preventDefault();
  var id = $(this).data('id');

    Swal.fire({
      title: 'Are you sure to client ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
        $.ajax({
        type: 'post',
        url: "{{route('user.client.update.admin')}}",
        data: {'_token':'{{ csrf_token() }}','id':id},
        success: function(response)
        {
          // console.log(response)
            location.reload();
        }
  });
  }
})
});
  });
</script>


@endsection