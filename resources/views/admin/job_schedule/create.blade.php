@extends('Admin.master')
@section('title','Add new job Schedule - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Create new job Schedule</h3>
  	<a href="{{route('job_schedule.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_schedule.submit')}}" method="post">
  		{{ csrf_field()}}

    <div class="form-group {{ $errors->has('schedule_name') ? 'has-error' : ''}}">
      <label for="schedulename">Job Schedule:</label>
      <input type="text" class="form-control" placeholder="schedule" name="schedule_name" value="{{old('schedule_name')}}" >
         @if($errors->has('schedule_name'))
            <span class="has-error">
                {{ $errors->first('schedule_name') }}
            </span>
        @endif


    </div>
    <div class="form-group">
      <label for="description">Slug:</label>
      <input type="text" class="form-control" id="text" placeholder="Slug" name="slug" value="{{old('slug')}}">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection