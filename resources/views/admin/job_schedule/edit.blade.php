@extends('Admin.master')
@section('title','Update job Schedule - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Update job Schedule</h3>
  	<a href="{{route('job_schedule.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_schedule.update',$job->id)}}" method="post">
  		{{ csrf_field()}}
    <div class="form-group {{ $errors->has('schedule_name') ? 'has-error' : ''}}">
      <label for="schedule_name">Job Location:</label>
      <input type="text" class="form-control" name="schedule_name" value="{{$job->schedule_name}}">

      @if($errors->has('schedule_name'))
            <span class="has-error">
                {{ $errors->first('schedule_name') }}
            </span>
        @endif
    </div>
    <div class="form-group">
      <label for="description">Slug:</label>
      <input type="text" class="form-control" id="text" value="{{$job->slug}}" name="slug">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection