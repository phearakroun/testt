@extends('Admin.master')
@section('title','All Job Schedule - ')
@section('content')

	<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">All Job Schedule</h3>
  	<a href="{{route('job_schedule.create')}}" class="btn btn-primary pull-right">New</a>
  </div>
  <div class="box-body">
  	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Schedule</th>
        <th>Slug</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $val)
      <tr>
        <th>{{ $val->id}}</th>
        <td>{{$val->schedule_name}}</td>
        <td>{{$val->slug}}</td>
        <td>
          
          <a href="{{route('job_schedule.edit',$val->id)}}" class="btn btn-primary btn-sm" >
                      <i class="glyphicon glyphicon-pencil"></i>
                    </a>

                   <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
              <i class="glyphicon glyphicon-trash"></i>
            </a>



        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <div class="pull-right">
  {{$data->links()}}
  </div>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');

          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('job_schedule.delete')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>

@endsection