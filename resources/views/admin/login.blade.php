<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>The Gate | Admin Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/square/blue.css')}}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
  

  <div class="login-box">
    <div class="login-logo">
        <img src="{{asset('admin/logo/logo.png')}}" height="80">
    </div>
    <!-- /.login-logo -->
    <div class="box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="{{route('thegate.login.submit')}}" method="post">
          {{ csrf_field()}}
          <div class='form-group'>
            <label class='control-label' for='email'>Email address</label> 
            <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id='email' name='email' placeholder='Email address' type='text' value="{{old('email')}}">
            @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
  
          </div>
          <div class='form-group'>
            <label class='control-label' for='password'>Password</label> 
            <input class="form-control w3l {{ $errors->has('password') ? ' is-invalid' : '' }}" id='password' name='password' placeholder='Password' type='password'>
  
            @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
          </div>
          <div class='submit'>
            <input class='btn btn-primary pull-right' type='submit' value='login'>
          </div>
        </form>

    </div>
</div>


<!-- jQuery 3 -->
<script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('admin/plugins/iCheck/icheck.min.js')}}"></script>
	</body>
</html>