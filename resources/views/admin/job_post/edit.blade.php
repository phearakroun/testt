@extends('Admin.master')
@section('title','Update Post Job - ')
@section('content')
<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Update Post Job</h3>
    <a href="{{route('post_job.admin.index')}}" class="btn btn-info pull-right">Back</a></div>
    <div class="panel-body">
      <form action="{{route('post_job.update',$postjob->id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field()}}
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label for="categories">Job Categories</label>
              <select class="form-control" id="location" name="categories">

                @foreach($jocate as $val)
                <option value="{{$val->id}}" {{$postjob->category_id == $val->id  ? 'selected' : ''}}>{{$val->jobcategoriesname}}</option>
                @endforeach


          

              </select>
            </div>

            <div class="form-group {{ $errors->has('jobtitle') ? 'has-error' : ''}}">
              <label for="jobtitle">Job Title:</label>
              <input type="text" class="form-control" name="jobtitle" value="{{$postjob->job_title}}">

               @if($errors->has('jobtitle'))
                <span class="has-error">
                  {{ $errors->first('jobtitle') }}
                </span>
                @endif
            </div>
            <div class="form-group ">
              <label for="company">Comapny:</label>
              
              <select class="form-control" id="company" name="company">

                @foreach($comp as $val)
                <option value="{{$val->id}}" {{$postjob->company_id == $val->id  ? 'selected' : ''}}>{{$val->company_name}}</option>
                @endforeach


               
              </select>
            </div>
            <div class="form-group ">
              <label for="joblocation">Location:</label>
              
              <select class="form-control" id="location" name="joblocation">

                   @foreach($jobLoc as $val)
                <option value="{{$val->id}}" {{$postjob->location_id == $val->id  ? 'selected' : ''}}>{{$val->joblocation}}</option>
                @endforeach

              </select>
            </div>
              <div class="form-group">
                <label for="schedule">Schedule:</label>
                
                <select class="form-control" id="schedule" name="schedule">

                      @foreach($jobsched as $val)
                <option value="{{$val->id}}" {{$postjob->schedule_id == $val->id  ? 'selected' : ''}}>{{$val->schedule_name}}</option>
                @endforeach


               
                </select>
              </div>
              
              <div class="form-group {{ $errors->has('salary') ? 'has-error' : ''}}">
                <label for="salary">Salary:</label>
                <input type="number" class="form-control" id="text" name="salary" value="{{$postjob->salary}}">
                @if($errors->has('salary'))
                <span class="has-error">
                  {{ $errors->first('salary') }}
                </span>
                @endif
              </div>
            
          </div>
          <div class="col-md-6">
            
            <div class="form-group {{ $errors->has('close_date') ? 'has-error' : ''}}">
                <label for="close_date">Close Date:</label>
                <input type="date" class="form-control" id="text" name="close_date" value="{{$postjob->close_date}}">
                @if($errors->has('close_date'))
                <span class="has-error">
                  {{ $errors->first('close_date') }}
                </span>
                @endif
              </div>


            <div class="form-group {{ $errors->has('duties') ? 'has-error' : ''}}">
              <label for="duties">Duties:</label>
              <textarea type="text" class="form-control" name="duties" rows="6">{{$postjob->duties}}</textarea>
              @if($errors->has('duties'))
              <span class="has-error">
                {{ $errors->first('duties') }}
              </span>
              @endif
            </div>
            <div class="form-group  {{ $errors->has('requirement') ? 'has-error' : ''}}">
              <label for="requirement">Requirement:</label>
              <textarea type="text" class="form-control" name="requirement" rows="7">{{$postjob->requirements}}</textarea>
              @if($errors->has('requirement'))
              <span class="has-error">
                {{ $errors->first('requirement') }}
              </span>
              @endif

            </div>
            
          </div>
        </div>
        
        
        <button type="submit" class="btn btn-primary pull-right">Save</button>
        
      </form>
    </div>
  </div>
  @endsection