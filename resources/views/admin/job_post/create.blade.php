@extends('Admin.master')
@section('title','Add new Post Job - ')
@section('content')
<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Create Post Job</h3>
    <a href="{{route('post_job.index')}}" class="btn btn-info pull-right">Back</a></div>
    <div class="panel-body">
      <form action="{{route('post_job.submit')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field()}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="categories">Job Categories</label>
              <select class="form-control" id="location" name="categories">
                @foreach($jocate as $val)
                <option value="{{$val->id}}">{{$val->jobcategoriesname}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group {{ $errors->has('jobtitle') ? 'has-error' : ''}}">
              <label for="jobtitle">Job Title:</label>
              <input type="text" class="form-control" name="jobtitle" placeholder="job title">

               @if($errors->has('jobtitle'))
                <span class="has-error">
                  {{ $errors->first('jobtitle') }}
                </span>
                @endif
            </div>
            <div class="form-group ">
              <label for="company">Comapny:</label>
              
              <select class="form-control" id="company" name="company">
                @foreach($comp as $val)
                <option value="{{$val->id}}">{{$val->company_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group ">
              <label for="joblocation">Location:</label>
              
              <select class="form-control" id="location" name="joblocation">
                @foreach($jobLoc as $val)
                <option value="{{$val->id}}">{{$val->joblocation}}</option>
                @endforeach
              </select>
            </div>
              <div class="form-group">
                <label for="schedule">Schedule:</label>
                
                <select class="form-control" id="schedule" name="schedule">
                  @foreach($jobsched as $val)
                  <option value="{{$val->id}}">{{$val->schedule_name}}</option>
                  @endforeach
                </select>
              </div>
              
              <div class="form-group {{ $errors->has('salary') ? 'has-error' : ''}}">
                <label for="salary">Salary:</label>
                <input type="number" class="form-control" id="text" name="salary" placeholder="00.00 $">
                @if($errors->has('salary'))
                <span class="has-error">
                  {{ $errors->first('salary') }}
                </span>
                @endif
              </div>
            
          </div>
          <div class="col-md-6">
            
            <div class="form-group {{ $errors->has('close_date') ? 'has-error' : ''}}">
                <label for="close_date">Close Date:</label>


                <!-- <input type="text" id="txtdate"> -->

              <!--   <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div> -->


                <input type="text" class="form-control" id="txtdate" name="close_date" placeholder="close date">

             <!--    <div class="form-group">
                    <div class="input-group date" id="datetimepicker">
                        <input type="text" class="form-control" />
                        <span class="input-group-addon">
                         <span class="glyphicon-calendar glyphicon"></span>
                       </span>
                    </div>
                </div> -->

      <!--           
            <div class="input-group date" id="datetimepicker">
                <input type="text" class="form-control" />  <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
            </div>
        </div> -->




                @if($errors->has('close_date'))
                <span class="has-error">
                  {{ $errors->first('close_date') }}
                </span>
                @endif
              </div>


            <div class="form-group {{ $errors->has('duties') ? 'has-error' : ''}}">
              <label for="duties">Duties:</label>
              <textarea type="text" class="form-control" placeholder="duties" name="duties" rows="6"></textarea>
              @if($errors->has('duties'))
              <span class="has-error">
                {{ $errors->first('duties') }}
              </span>
              @endif
            </div>
            <div class="form-group  {{ $errors->has('requirement') ? 'has-error' : ''}}">
              <label for="requirement">Requirement:</label>
              <textarea type="text" class="form-control" placeholder="requirement" name="requirement" rows="7"></textarea>
              @if($errors->has('requirement'))
              <span class="has-error">
                {{ $errors->first('requirement') }}
              </span>
              @endif

            </div>
            
          </div>
        </div>
        
        <button type="submit" class="btn btn-primary pull-right">Save</button>
        
      </form>
    </div>
  </div>


<script language="javascript">
        $(document).ready(function () {
            $("#txtdate").datepicker({
                minDate: 0
            });
        });
</script>



  
  
  @endsection




  
