@extends('Admin.master')
@section('title','All Job Categories - ')
@section('content')

	<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">All Job Location</h3>
  	<a href="{{route('job_location.create')}}" class="btn btn-primary pull-right">New</a>
  </div>
  <div class="box-body">
  	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Job Location</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $val)
      <tr>
        <th>{{$val->id}}</th>
        <td>{{$val->joblocation}}</td>
        <td>{{$val->jobdescription}}</td>
        <td>
          
          <a href="{{route('job_location.edit',$val->id)}}" class="btn btn-primary btn-sm" >
                      <i class="glyphicon glyphicon-pencil"></i>
                    </a>

                    <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
              <i class="glyphicon glyphicon-trash"></i>
            </a>



        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <div class="pull-right">
  {{$data->links()}}
  </div>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');

          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('job_location.delete')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>


@endsection