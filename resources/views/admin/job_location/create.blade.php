@extends('Admin.master')
@section('title','Add new job Location - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Create new job location</h3>
  	<a href="{{route('job_location.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_location.submit')}}" method="post">
  		{{ csrf_field()}}

    <div class="form-group {{ $errors->has('joblocation') ? 'has-error' : ''}}">
      <label for="joblocation">Job Location:</label>
      <input type="text" class="form-control" placeholder="Job location" name="joblocation" value="{{old('joblocation')}}" >
         @if($errors->has('joblocation'))
            <span class="has-error">
                {{ $errors->first('joblocation') }}
            </span>
        @endif


    </div>
    <div class="form-group">
      <label for="description">Description:</label>
      <input type="text" class="form-control" id="text" placeholder="Description" name="description" value="{{old('description')}}">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection