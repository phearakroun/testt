@extends('Admin.master')
@section('title','Update job location - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Update job location</h3>
  	<a href="{{route('job_location.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_location.update',$job->id)}}" method="post">
  		{{ csrf_field()}}
    <div class="form-group {{ $errors->has('joblocation') ? 'has-error' : ''}}">
      <label for="joblocation">Job Location:</label>
      <input type="text" class="form-control" name="joblocation" value="{{$job->joblocation}}">

      @if($errors->has('joblocation'))
            <span class="has-error">
                {{ $errors->first('joblocation') }}
            </span>
        @endif
    </div>
    <div class="form-group">
      <label for="description">Description:</label>
      <input type="text" class="form-control" id="text" value="{{$job->jobdescription}}" name="description">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection