@extends('Admin.master')
@section('title','Apply job - ')
@section('content')

	<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">User Apply Job</h3>

  </div>
  <div class="box-body">
  	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>No</th>
        
        <th>Job title</th>
        <th>Phone</th>
        <th>CV</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     @foreach ($data as $val )

     <tr>
     <th>{{$val->id}}</th>
      <td>{{$val->job_title}}</td>
      
      <td>{{$val->phone}}</td>
         <td>
        
         <a href="{{route('view.cv',$val->id)}}" class="btn btn-info btn-sm" target="_blank" >
                    <i class="glyphicon glyphicon-eye-open"></i> View CV
                  </a>
      </td>

      <td>
        
        <a href="" class="btnDelteCom btn btn-danger btn-sm" data-id="{{$val->id}}">
          <i class="glyphicon glyphicon-trash"></i>
        </a>
      </td>
    </tr>
         
     @endforeach
      
 
    </tbody>
  </table>
  <div class="pull-right">
  </div>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(".btnDelteCom").click(function(e) {

      e.preventDefault();
        var id = $(this).data('id');
          Swal.fire({
            title: 'Are you sure to delete ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
              $.ajax({
              type: 'post',
              url: "{{route('view.cv.deletes')}}",
              data: {'_token':'{{ csrf_token() }}','id':id},
              success: function(response)
              {
                  location.reload();
              }
        });
        }
      })
    });
  });
</script>


@endsection