@extends('Admin.master')
@section('title','Admin Dashboard - ')
@section('content')
	<div class="box box-primary">
  <div class="box-header with-border">Admin Dashboard</div>
  <div class="box-body">
  	
  	<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
     
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{ $post }}</h3>
            <p>Job</p>
          </div>
          <div class="icon">
            <i class="ion ion-android-folder"></i>
          </div>
 
        
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
    
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{ $cate }}</h3>
            <p>Job Categories</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
    
       
      <a href="{{ route('job_categories.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
     
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{ $locat }}</h3>
            <p>Job Location</p>
          </div>
          <div class="icon">
            <i class="ion ion-map"></i>
          </div>
         
       
     <a href="{{ route('job_location.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">

    	<div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $comp }}</h3>

              <p>Companies</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-globe"></i>
            </div>
            <a href="{{ route('companies.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>


      
    </div>
    <!-- ./col -->
  </div>



</section>

  </div>
</div>
@endsection