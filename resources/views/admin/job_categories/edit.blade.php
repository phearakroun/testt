@extends('Admin.master')
@section('title','Update job categories - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Update job categories</h3>
  	<a href="{{route('job_categories.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_categories.update',$job->id)}}" method="post">
  		{{ csrf_field()}}
    <div class="form-group {{ $errors->has('jobcategoriesname') ? 'has-error' : ''}}">
      <label for="jobcategoriesname">Job Categories Name:</label>
      <input type="text" class="form-control" name="jobcategoriesname" value="{{$job->jobcategoriesname}}">

      @if($errors->has('jobcategoriesname'))
            <span class="has-error">
                {{ $errors->first('jobcategoriesname') }}
            </span>
        @endif
    </div>
    <div class="form-group">
      <label for="description">Description:</label>
      <input type="text" class="form-control" id="text" value="{{$job->jobdescription}}" name="description">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection