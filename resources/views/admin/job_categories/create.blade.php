@extends('Admin.master')
@section('title','Add new job categories - ')
@section('content')

	<div class="row">
	
		<div class="col-md-8">
			
			<div class="box box-primary">
  <div class="box-header with-border"><h3 class="box-title">Create new job categories</h3>
  	<a href="{{route('job_categories.index')}}" class="btn btn-info pull-right">Back</a></div>
  <div class="panel-body">
  	<form action="{{route('job_categories.submit')}}" method="post">
  		{{ csrf_field()}}

    <div class="form-group {{ $errors->has('jobcategoriesname') ? 'has-error' : ''}}">
      <label for="jobcategoriesname">Job Categories Name:</label>
      <input type="text" class="form-control" placeholder="Job categories name" name="jobcategoriesname" value="{{old('jobcategoriesname')}}" >
         @if($errors->has('jobcategoriesname'))
            <span class="has-error">
                {{ $errors->first('jobcategoriesname') }}
            </span>
        @endif


    </div>
    <div class="form-group">
      <label for="description">Description:</label>
      <input type="text" class="form-control" id="text" placeholder="Description" name="description" value="{{old('description')}}">
    </div>

    <button type="submit" class="btn btn-primary pull-right">Save</button>
  
  </form>
  </div>
</div>


		</div>
		<div class="col-md-4"></div>
	</div>

	
@endsection