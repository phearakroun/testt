  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>



      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Admin Dashboard</li>
                <li>
                    <a href="{{asset('/thegate')}}">
                        <i class="fa fa-dashboard text-aqua"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-group text-red"></i> <span>User Management</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('user.admin')}}"><i class="fa fa-user text-red"></i>User Admin</a></li>
                        <li><a href="{{route('user.client.index')}}"><i class="fa fa-user text-blue"></i><span>User Client</span></a></li>
                    </ul>
                </li>

                
                <li class="treeview">
                    <a href="#"><i class="fa fa-search text-blue"></i> <span>Job</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('job_categories.index')}}"><i class="fa fa-sitemap text-blue"></i>Job Category</a></li>
                        <li><a href="{{route('job_location.index')}}"><i class="fa fa-map-marker text-blue"></i>Job Location</a>
                        </li>
                        <li><a href="{{route('job_schedule.index')}}"><i class="fa fa-share text-blue"></i>Job Schedule</a></li>
                    </ul>
                </li>
                <li><a href="{{route('companies.index')}}"><i class="fa fa-handshake-o text-red"></i> <span>Company</span></a></li>
                
                {{-- <li><a href="{{route('apply.job.index')}}"><i class="fa fa-send-o text-blue"></i><span>Apply Job</span></a></li> --}}


                <li class="treeview">
                  <a href="#"><i class="fa fa-group text-red"></i> <span>Job Type</span>
                      <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{route('post_job.admin.index')}}"><i class="fa fa-file-o text-green"></i><span>Job Admin</span></a></li>
                      <li><a href="{{route('post_job.client.index')}}"><i class="fa fa-file-o text-green"></i><span>Job Client</span></a></li>
                  </ul>
              </li>

              
               
            </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>