@extends('layouts.app')
@section('search')
<div class="site-blocks-cover overlay" style="background-image:url({{url('front/images/hero_1.jpg')}})" data-aos="fade" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12" data-aos="fade">
        <h1>Find All Jobs In Cambodia</h1>
        <form action="#">
          <div class="row mb-3">
            <div class="col-md-9">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                        <input name="name" style="height: 55px !important;" type="text" class="form-control border-0 input-block"
                    placeholder="job title, keywords  " value="{{$f_name}}">
                    </div>
                    <div class="col-md-4">
                      {{-- <label for="">Select Categories</label> --}}
                      <select  type="text" class="form-control" name="category" style="height: 55px !important;">
                        <option value="">Select Category</option>
                        @foreach ($categories as $com)
                        <option value="{{$com->id}}"  {{$f_category == $com->id  ? 'selected' : ''}}>{{$com->jobcategoriesname}} <span>({{$com->jobs->count()}})</span></option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-4">
                      <select type="text" class="form-control" name="location" style="height: 55px !important;" >
                        <option value="">Select Location</option>
                        @foreach ($locations as $com)
                        <option value="{{$com->id}}"  {{$f_location == $com->id  ? 'selected' : ''}}>{{$com->joblocation}} <span>({{$com->jobs->count()}})</span></option>
                        @endforeach
                      </select>
                    </div>
                  </div>
               
                </div>
              </div>
            </div>
            <div class="col-md-2">
             
                <input type="submit" class="btn btn-search btn-primary btn-block" value="Search">
     
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12 mb-5 mb-md-0" data-aos="fade-up" data-aos-delay="100">
    <h4>All Jobs Post In Cambodia</h4>

    <hr>
    <div class="jobs-wrap" >
      @if($jobs->count() < 0)
      <h5>No Data ..............</h5>
      @endif
      <div class="row">
        @foreach ( $jobs as $job)
        <div class="col-md-6">
        <a href="{{route('view',['id'=>$job->id])}}" class="job-item d-block d-md-flex align-items-center  border-bottom fulltime" style="border-left: 3px solid #26baee;
          border-right: 3px solid #eaeaea;
          padding-top: 10px;
          margin-top: 10px;">
            <div class="company-logo blank-logo text-center text-md-left pl-3">
              <img src="{{url('front/img-logo/logo.png')}}" alt="Image" class="img-fluid mx-auto">
            </div>
            <div class="job-details h-100">
              <div class="p-3 align-self-center">
                <h3>{{$job->job_title}}</h3>
                <div class="d-block d-lg-flex">
                <div class="mr-3"><span class="icon-suitcase mr-1">
                  </span>{{$job->location->joblocation}}</div>
                  <div  class="mr-3"><span class="icon-room mr-1"></span> Salary : </div>
                  <div><span  class="icon-money mr-1">$ {{floatval($job->salary)}} </span></div>
                </div>
              </div>
            </div>
            <div class="job-category align-self-center">
              <div class="p-3">
              <span class="text-info p-2 rounded border border-info">{{$job->schedule->schedule_name}}</span>
              </div>
            </div>
          </a>
        </div>
        @endforeach
       
      </div>
    </div>
    <div class="col-md-12 text-center mt-5">
      {{ $jobs->links() }}
      {{-- <a href="#" class="btn btn-primary rounded py-3 px-5"><span class="icon-plus-circle"></span> Show More Jobs</a> --}}
    </div>
  </div>

</div>
{{--  --}}
<hr>
<div class="site-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 mx-auto text-center mb-5 section-heading">
        <h2 class="mb-5">Popular Categories</h2>
      </div>
    </div>
    <div class="row">
      @foreach ($categories as $cat )
      <div class="col-sm-6 col-md-4 col-lg-3 mb-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
      <a href="{{route('jobs-categories-id',['id'=>$cat->id])}}" class="h-100 feature-item">
          {{-- <span class="d-block icon flaticon-calculator mb-3 text-primary"></span> --}}
        <h2>{{$cat->jobcategoriesname}}</h2>
        Job Posted <span class="counting">{{$cat->jobs->count()}}</span>
        </a>
      </div>
      @endforeach
    </div>

  </div>
</div>
<br>
{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 
<script type="text/javascript">
  //  var route = "{{ url('autocomplete') }}";
  //   $('.typeahead').typeahead({
  //       source:  function (term, process) {
  //       return $.get(route, { term: term }, function (data) {
  //               return process(data);
  //           });
  //       }
  //   });
</script>
@endsection